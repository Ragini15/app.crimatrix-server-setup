the steps necessary to set up server for app.crimatrix


## Step 1: Initial Server Setup on DigitalOcean ##

### Root Login ###
ssh root@droplet_ip_address

### Set LC_ALL and LC_LANGUAGE ###
nano /etc/environment

ADD
LC_ALL="en_US.UTF-8"
LC_LANGUAGE="en_US.UTF-8"

### Create New User ###
adduser ragini

password

### Set Root Priviledge ###
usermod -aG sudo ragini

### Add Public Key Authentication ###
ssh - keygen  [note:using puttygen to create the public and private key pair for windows]
su - ragini
mkdir .ssh
chmod 700 .ssh
nano .ssh/authorized_keys
sudo service ssh restart

TEST SSH WITH THE NEW USER

LOGIN AS NEW USER
###login as the added user
 ssh ragini@your_server_ip
 
### Configure Timezone ###
sudo dpkg-reconfigure tzdata

### Configure NTP Synchronization ###
sudo apt-get update
sudo apt-get install ntp

### Install Apache2 stack ###
sudo apt-get update
sudo apt-get install apache2
sudo apache2ctl configtest
=>Syntax OK
sudo nano /etc/apache2/apache2.conf

ADD THIS TO THE etc/apache2/apache2.conf 
ServerName server_domain_or_IP

sudo apache2ctl configtest
=>Syntax OK
sudo systemctl restart apache2

http://your_server_IP_address
You will see the default Ubuntu 16.04 Apache web page, which is there for informational and testing purposes.

###Install MySQL
sudo apt-get install mysql-server

### Install PHP ###
sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
sudo nano /etc/apache2/mods-enabled/dir.conf 
*bring index.php before index.html*
sudo service apache2 restart

### Test PHP Processing on server ###
sudo nano /var/www/html/info.php
ADD to /var/www/html/info.php
<?php
phpinfo();
?>

VISIT: http://droplet_ip_address/info.php

### Install phpMyAdmin ###
sudo apt-get install phpmyadmin apache2-utils

Once the process starts up, follow these steps:
Select Apache2 for the server
Choose YES when asked about whether to Configure the database for phpmyadmin with dbconfig-common
Enter your MySQL password when prompted
Enter the password that you want to use to log into phpmyadmin

sudo nano /etc/apache2/apache2.conf

ADD THIS TO THE etc/apache2/apache2.conf 
Include /etc/phpmyadmin/apache.conf

sudo service apache2 restart
###Accessing phpMyAdmin
http://droplet_ip_address/phpmyadmin

### Nginx as a Web Server and Reverse Proxy for Apache ###
Update your hosts file on local system with following domains

68.183.88.245 app.crimatrix.com
68.183.88.245 demo.crimatrix.com
The first static sites will be served by Nginx. The remaining dynamic sites will be served by Apache2. Nginx will also serve as a Reverse Proxy for Apache2 virtual hosts.

## Step 2: Setup Apache Virtual Hosts on DigitalOcean ##
These virtual hosts will serve:
   demo.crimatrix.com
   
### Create the Directory Structure ###
sudo mkdir -p /var/www/demo.crimatrix.com/public_html

### Grant Permissions ###
sudo chown -R $USER:$USER /var/www/demo.crimatrix.com/public_html
sudo chmod -R 755 /var/www

### Create Demo Pages for Each Virtual Host ###
nano /var/www/demo.crimatrix.com/public_html/index.html
nano /var/www/demo.crimatrix.com/public_html/info.php
### Configuring Apache virtual hosts to run on port 8080 ###
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/demo.crimatrix.com.conf
### Update Virtual Host file ServerAdmin ServerName ServerAlias DocumentRoot ###
sudo nano /etc/apache2/sites-available/demo.crimatrix.com.conf
### Enable the New Virtual Host Files ###
sudo a2ensite demo.crimatrix.com
sudo service apache2 restart
### Test your results
VISIT:
http://demo.crimatrix.com:8080    http://demo.crimatrix.com:8080/info.php

## Step 3: Install PHP-FPM and PHP FastCGI Apache module ##
sudo apt-get install libapache2-mod-fastcgi php5-fpm
### Configuring Apache to Use mod_fastcgi ###
sudo a2enmod actions

Check Apache2 version
sudo apache2 -v

sudo nano /etc/apache2/mods-enabled/fastcgi.conf
sudo apachectl -t
sudo service apache2 restart

## Step 4: Setup Nginx server blocks (Virtual Hosts) ##
These server blocks (or virtual hosts) will serve:
  app.crimatrix.com
### Install Nginx ###
sudo apt-get install nginx
### Create virtualhost Document Directories ###
sudo mkdir -p /var/www/app.crimatrix.com/html
### Grant Permissions ###
sudo chown -R $USER:$USER /var/www/app.crimatrix.com/html
sudo chmod -R 755 /var/www
### Create Demo Pages for Each Virtual Host ###
nano /var/www/demo.crimatrix.com/html/index.html
nano /var/www/demo.crimatrix.com/html/info.php
### Create the First Server Block File ###
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/app.crimatrix.com
### Update Virtual Host file ServerAdmin ServerName ServerAlias Root ###
sudo nano /etc/nginx/sites-available/app.crimatrix.com
### Enable your Server Blocks and Restart Nginx ###
sudo ln -s /etc/nginx/sites-available/app.crimatrix.com /etc/nginx/sites-enabled/app.crimatrix.com
### Remove the # symbol to uncomment the line
sudo nano /etc/nginx/nginx.conf
     server_names_hash_bucket_size 64;

sudo nginx -t
sudo systemctl restart nginx
### Test your results
VISIT:
http://app.crimatrix.com:8080    http://app.crimatrix.com:8080/info.php
### Configuring Nginx for Apache's Virtual Hosts ###
sudo nano /etc/nginx/sites-available/apache
[ADD a code block in the above file and update the server name,server alias and proxy_pass,proxy_host]
sudo ln -s /etc/nginx/sites-available/apache /etc/nginx/sites-enabled/apache
sudo nginx -t
sudo systemctl reload nginx
### Test your results
VISIT:
http://demo.crimatrix.com   http://demo.crimatrix.com/info.php
### Installing and Configuring mod_rpaf ###
Without this module, some PHP applications would require code changes to work seamlessly from behind a proxy.

sudo apt-get install unzip build-essential apache2-dev
wget https://github.com/gnif/mod_rpaf/archive/stable.zip
unzip stable.zip
cd mod_rpaf-stable
make
sudo make install
sudo nano /etc/apache2/mods-available/rpaf.load

ADD
LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so

sudo nano /etc/apache2/mods-available/rpaf.conf
sudo a2enmod rpaf
sudo service apache2 restart

## Step 5: Installing Let's Encrypt SSL Certificate ##
Before Proceeding make sure DNS Record is set as in reference image.
NameServers MUST be pointed to DigitalOcean DNS
### Installing certbot
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-certbot-nginx
### Confirm nginx configuration
sudo nano /etc/nginx/sites-available/app.crimatrix.com
sudo nginx -t
sudo systemctl reload nginx
### Obtaining an SSL Certificate
sudo certbot --nginx -d app.crimatrix.com -d www.app.crimatrix.com
sudo certbot --nginx -d demo.crimatrix.com -d www.demo.crimatrix.com
### Verifying Certbot Auto-Renewal
sudo certbot renew --dry-run
### Generate Strong Diffie-Hellman Group to increase ###
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
### Configure TLS/SSL on Web Server (Nginx) ###
Comment out or delete the lines that configure the server block to listen on port 80. Configure these server block to listen on port 443 with SSL enabled instead.
nano /etc/nginx/sites-available/app.crimatrix.com
### Test and restart Nginx ###
nginx -t
service nginx restart
### Test SSL Certificates ###
https://www.ssllabs.com/ssltest/analyze.html?d=app.crimatrix.com
https://www.ssllabs.com/ssltest/analyze.html?d=demo.crimatrix.com



